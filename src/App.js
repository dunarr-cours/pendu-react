import React, {useState} from 'react';
import logo from './logo.svg';
import './App.css';


function App() {
    const wordList = [
        "javascript",
        "biere",
        "skyrim"
    ]
    const alphabet = "abcdefghijklmnopqrstuvwxyz"
    const [word, setWord] = useState(getRandomWord())
    const [usedLetters, setUsedLetters] = useState( "")
    const [lives, setLives] = useState(5);
    function getRandomWord() {
        return wordList.sort(()=>Math.random()-.5)[0]
    }
    function renderWord() {
        return word.split("").map((letter, index)=> <span key={index}> {usedLetters.indexOf(letter)>-1 || gameEnd()?letter:"_"} </span>)
    }
    function renderAlphabet() {
        return alphabet.split("").map((letter, index)=><button disabled={usedLetters.indexOf(letter) > -1 || gameEnd()} key={index} onClick={tryLetter(letter)}>{letter}</button>)
    }
    function renderLives() {
        if(hasWin()){
            return <span>gagné!</span>
        }
        if(hasLost()){
            return <span>perdu!</span>
        }
        return <span>Vies: {new Array(lives).fill().map((valiue, index)=><span key={index}> * </span>)}</span>
    }
    function hasLost(){
        return lives <=0
    }
    function hasWin(){
        let win = true
        word.split("").forEach(letter=>{
            if(usedLetters.indexOf(letter) === -1) {
                win = false
            }
        })
        return win
    }
    function gameEnd() {
        return hasWin() || hasLost();
    }
    function tryLetter(letter) {
        return ()=>{
            if(usedLetters.indexOf(letter) === -1){
                setUsedLetters(usedLetters+letter)
                if(word.indexOf(letter) === -1) {
                    setLives(lives-1)
                }
            }
        }
    }
    function startGame() {
        setLives(5)
        setUsedLetters("")
        setWord(getRandomWord())
    }
    return (
        <div className="App">
            <div className="lives">
                {renderLives()}
            </div>
            <div className="word">
                {renderWord()}
            </div>
            <br/>
            <div className="alphabet">
                {renderAlphabet()}
            </div>
            {gameEnd()? <button onClick={startGame}>Recommencer</button>:null}
        </div>
    );
}

export default App;
